import React, { Component } from 'react';
import { Button, Container } from 'reactstrap';
import UserListEditor from './userListEditor';
import UserListViewer from './userListViewer';
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';

class App extends Component {
  state = {
    onOpen: null,
  };

  render() {
    const { onOpen } = this.state;

    return (
      <Container fluid>
        <h1 className="text-center">BRUGERLISTE</h1>
        <UserListViewer/>
        <Button
          className="my-2"
          color="light"
          onClick={() => this.setState({ onOpen: {} })}
        >
          &#43; Tilf&oslash;j bruger
        </Button>
        {onOpen && (
          <UserListEditor
            user={onOpen}
            onClose={() => this.setState({ onOpen: null })}
          />
        )}
      </Container>
    );
  }
}

export default App;