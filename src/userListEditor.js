import React from 'react';
import gql from 'graphql-tag';
import {
  Button,
  Form,
  FormGroup,
  Label,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from 'reactstrap';
import { Form as FinalForm, Field } from 'react-final-form';
import client from './apollo';
import { GET_USERS } from './userListViewer';

const SUBMIT_USER = gql`
  mutation SubmitUser($input: UserInput!) {
    submitUser(input: $input) {
      id
    }
  }
`;

const UserListEditor = ({ user, onClose }) => (
  <FinalForm
    onSubmit={async ({ id, name, email }) => {
      const input = { id, name, email };

      await client.mutate({
        variables: { input },
        mutation: SUBMIT_USER,
        refetchQueries: () => [{ query: GET_USERS }],
      });

      onClose();
    }}
    initialValues={user}
    render={({ handleSubmit, pristine, invalid }) => (
      <Modal isOpen toggle={onClose}>
        <Form onSubmit={handleSubmit}>
          <ModalHeader toggle={onClose}>
            {user.id ? 'Ret bruger' : 'Ny bruger'}
          </ModalHeader>
          <ModalBody>
            <FormGroup>
              <Label>Navn</Label>
              <Field
                required
                name="name"
                className="form-control"
                component="input"
              />
            </FormGroup>
            <FormGroup>
              <Label>Email</Label>
              <Field
                required
                name="email"
                className="form-control"
                component="input"
              />
            </FormGroup>
          </ModalBody>
          <ModalFooter>
            <Button type="submit" disabled={pristine} color="primary">Gem</Button>
            <Button color="secondary" onClick={onClose}>Fortryd</Button>
          </ModalFooter>
        </Form>
      </Modal>
    )}
  />
);

export default UserListEditor;