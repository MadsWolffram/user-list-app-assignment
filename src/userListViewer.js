import React from 'react';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';
import { Table } from 'reactstrap';
import { Button, Form } from 'reactstrap';
import client from "./apollo";
import { Form as FinalForm} from 'react-final-form';

export const GET_USERS = gql`
  query GetUsers {
    users {
      id
      name
      email
    }
  }
`;

const DELETE_USER = gql`
  mutation DeleteUser($id: ID!) {
    deleteUser(id: $id) {
        id
    }
  }
`;

const UserListViewer = () => (
    <Query query={GET_USERS}>
        {({ loading, data }) => !loading && (
            <Table className="table table-bordered">
                <thead className="">
                    <tr>
                        <th>#</th>
                        <th>Navn</th>
                        <th>Email</th>
                        <th>Slet</th>
                    </tr>
                </thead>
                <tbody>
                    {data.users.map(user => (
                        <tr key={user.id}
                        >
                            <td>{user.id}</td>
                            <td>{user.name}</td>
                            <td>{user.email}</td>
                            <td>
                                <FinalForm
                                    onSubmit={async ({ id }) => {
                                        await client.mutate({
                                            variables: { id },
                                            mutation: DELETE_USER,
                                            refetchQueries: () => [{ query: GET_USERS }],
                                        });
                                    }}
                                    initialValues={user}
                                    render={({ handleSubmit, pristine, invalid }) => (
                                        <Form onSubmit={handleSubmit}>
                                            <Button type="submit" className="btn btn-danger">Slet</Button>
                                        </Form>
                                    )}
                                />
                            </td>
                        </tr>
                    ))}
                </tbody>
            </Table>
        )}
    </Query>
);

export default UserListViewer;