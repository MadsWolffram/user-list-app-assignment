const express = require('express');
const cors = require('cors');
const graphqlHTTP = require('express-graphql');
const gql = require('graphql-tag');
const { buildASTSchema } = require('graphql');

const USERS = [
  { name: "John Doe", email: "john@doe.dk"},
  { name: "Richard Row ", email: "richard@roe.dk" },
  { name: "Jane Doe", email: "jane@doe.dk" },
  { name: "Joe Public", email: "joe@public.dk"},
  { name: "Janie Roe", email: "janie@roe.dk" },
  { name: "John Smith", email: "john@smith.dk"},
];

const schema = buildASTSchema(gql`
  type Query {
    users: [User]
    user(id: ID!): User
  }

  type User {
    id: ID
    name: String
    email: String
  }

  type Mutation {
    submitUser(input: UserInput!): User
    deleteUser(id: ID!): User!
  }
  
  input UserInput {
    id: ID
    name: String!
    email: String!
  }
`);

const mapUser = (user, id) => user && ({ id, ...user });

const root = {
  users: () => USERS.map(mapUser),
  user: ({ id }) => mapUser(USERS[id], id),
  submitUser: ({ input: { id, name, email } }) => {
    const user = { name, email };
    let index = USERS.length;
  
    if (id != null && id >= 0 && id < USERS.length) {
      if (USERS[id].userId !== userId) return null;
  
      USERS.splice(id, 1, user);
      index = id;
    } else {
      USERS.push(user);
    }
  
    return mapUser(user, index);
  },
  deleteUser: ({id}) => {
    let user = mapUser(USERS[id], id);
    USERS.splice(id, 1);

    return user;
  },
};

const app = express();
app.use(cors());
app.use('/graphql', graphqlHTTP({
  schema,
  rootValue: root,
  graphiql: true,
}));

const port = process.env.PORT || 4000
app.listen(port);
console.log(`Running GraphQL API server at localhost:${port}/graphql`);